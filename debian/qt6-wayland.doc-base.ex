Document: qt6-wayland
Title: Debian qt6-wayland Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-wayland is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-wayland/qt6-wayland.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-wayland/qt6-wayland.ps.gz

Format: text
Files: /usr/share/doc/qt6-wayland/qt6-wayland.text.gz

Format: HTML
Index: /usr/share/doc/qt6-wayland/html/index.html
Files: /usr/share/doc/qt6-wayland/html/*.html
